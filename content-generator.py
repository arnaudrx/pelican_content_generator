import random
import time
from itertools import cycle
from slugify import slugify
import os
import shutil


CONTENT_DIR = '../content/'
CURENT =  os.path.dirname(os.path.realpath(__file__))
category_tags = {
    'code': ['python', 'ruby', 'perl', 'haskell'],
    'database': ['postgres', 'sqlite3', 'mongoDB'],
    'admin-sys': ['debian', 'redhat', 'slackware'],
    'workflow': ['teamwork', 'software'],

}
authors_list = ['Arnaud R', 'Carlos', 'Guido']
try:
    shutil.rmtree(CONTENT_DIR)
    os.makedirs(directory)
except:
    pass
def create_title_list(default = 'title_data.txt'):
   with open(default, 'r') as fil:
      s = fil.read()

   r = []
   p = []
   for i in s.splitlines():
      if i:
         try:
            int(i[0])
            fr = '-'.join(i.split('-')[1:])
            p.append(fr)
         except ValueError:
            en = i
            r.append(en)
   return [r, p, ]

def iter_title():
    title_list = create_title_list()
    return ( (p, q,) for p, q in zip(title_list[0],title_list[1]))

ppoiuy = iter_title()

def generate_title(ppoiuy =ppoiuy ):
    return next(ppoiuy)

def shuffle_and_tear( list_obj):
    return random.sample(list_obj, random.randint(0, len(list_obj)))

def random_date(start, end):
    format = '%Y-%m-%d %H:%M'
    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))
    ptime = stime + random.random() * (etime - stime)
    return time.strftime(format, time.localtime(ptime))


template = """{title}
################################################################################################
:lang: {lang}
:date: {date}
:modified: {modified}
:tags: {tags}
:author: {authors}
:image: {image}

Title are underlined with equals signs too
==========================================

Subtitles with dashes
---------------------

.. PELICAN_BEGIN_SUMMARY

Here a summary of an article example.

.. PELICAN_END_SUMMARY

You can  put text in *italic* or in **bold**, you can "mark" text as code with double backquote ``print()``.

Lists are as simple as in Markdown:

- First item
- Second item
    - Sub item

or

* First item
* Second item
    * Sub item

Tables are really easy to write:

=========== ========
Country     Capital
=========== ========
France      Paris
Japan       Tokyo
=========== ========

More complex tables can be done easily (merged columns and/or rows) but I suggest you to read the complete doc for this :)

There are multiple ways to make links.

"""

frtemplate = """{title}
################################################################################################
:lang: {lang}
:date: {date}
:modified: {modified}
:tags: {tags}
:author: {authors}
:image: {image}

Les titres sont soulignés par des signes égaux aussi
======================================================

Sous-titres avec tirets
------------------------

.. PÉLICAN_DÉBUT_SOMMAIRE

Voici un résumé d'un exemple d'article.

.. PÉLICAN_END_SOMMAIRE

Vous pouvez mettre du texte en *italic* ou en **bold****, vous pouvez "marquer" le texte comme code avec une double citation au verso ``print()``.

Les listes sont aussi simples que dans Markdown :

- Premier élément
- Deuxième point
    - Sous-poste

ou

* Premier article
* Deuxième élément
    * Sous-poste

Les tableaux sont très faciles à écrire :

=========== ========
Capitale du pays
=========== ========
France      Paris
Japon       Tokyo
=========== ========

Des tableaux plus complexes peuvent être faits facilement (colonnes et/ou lignes fusionnées) mais je vous suggère de lire la documentation complète pour cela :)

Il y a plusieurs façons de faire des liens.

"""

all_lang = ['fr', 'en']
for i in range(1, 20):
    if i%2 == 1:
        lang = 'en'
    else:
        lang = 'fr'
    if lang == 'en':
        date = random_date("2018-12-11 20:26", "2019-06-11 18:30")
        modified = random_date(date, "2019-07-11 18:30")
        category = random.choice(list(category_tags.keys()))
        title_fr_en = generate_title()
        title = title_fr_en[0] + ' ' + str(i)
        article_nb = i
        slug = slugify(title+ str(article_nb) )
        #slug = slugify(title)
        authors = random.choice(authors_list)
        summary = title
        image  = "{{photo}}{0}".format(random.choice(os.listdir(CURENT + '/contentimages')))
        tags = ','.join(category_tags[category])
        ftemplate = eval('f\"\"\"'+ template + '\"\"\"')
    else:
        title = title_fr_en[1] + ' ' + str(i)
        article_nb = i - 1
        ftemplate = eval('f\"\"\"'+ frtemplate + '\"\"\"')
    article_dir = os.path.join(CONTENT_DIR, category+'/')
    try:
        os.makedirs(CONTENT_DIR + article_dir)
    except:
        pass



    with open(article_dir + str(article_nb)+ '_' + lang+ '.rst', 'w') as fil:
        fil.write(ftemplate)
