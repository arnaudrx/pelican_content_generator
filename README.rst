==========================
Pelican content generator
==========================


A simple and dirty content generator for pelican

Installation
--------------

Clone project in your pelican project:

.. code-block:: shell

  git clone --recursive https://arnaudrx@bitbucket.org/qanop/hexack-blog.git
  pip install -r requirements.txt

This conten use plugin, install them by:

Adding in your pelicanconf.py:

.. code-block:: python

  # Plugins configuration
    PLUGIN_PATHS = ['plugins/pelican-plugins',]
    PLUGINS = [ 'i18n_subsites',
                'photos',
                'summary',
              ]

    # I18N subsite configuration

    DEFAULT_LANG = 'fr'
    LOCALE = 'fr_FR.UTF-8'
    DEFAULT_DATE_FORMAT = ('%d %B %Y')

    JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}

    ARTICLE_TRANSLATION_ID = 'slug'
    PAGE_TRANSLATION_ID = 'slug'

    I18N_SUBSITES = {
        'en': {
            'SITENAME': SITENAME,
            'LOCALE': 'en_US.UTF-8',
            'I18N_UNTRANSLATED_PAGES': 'remove',
            'I18N_UNTRANSLATED_ARTICLES': 'remove',
            'ROOT_LANG_URL' : '/en',
            },
         'fr': {
            'SITENAME': SITENAME,
            'LOCALE': 'fr_FR.UTF-8',
            'I18N_UNTRANSLATED_PAGES': 'remove',
            'I18N_UNTRANSLATED_ARTICLES': 'remove',
            'ROOT_LANG_URL' : '',
            },

        }
    # Photo configuration
    PHOTO_LIBRARY = './page-creator/contentimages'


Simply make:

  python content-generator.py

**Warning**: your content dir will be erased.

Why use this script ?
----------------------

When you create a multilang theme for pelican you will want to have lot of content for testing internationalysation and pagination.
